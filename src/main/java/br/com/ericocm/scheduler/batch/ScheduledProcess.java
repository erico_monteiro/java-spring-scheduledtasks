package br.com.ericocm.scheduler.batch;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@EnableScheduling
public class ScheduledProcess {
    private final long SEGUNDO = 1000;
    private final long MINUTO = SEGUNDO * 60;
    private final long HORA = MINUTO * 60;

    @Scheduled(fixedDelay = SEGUNDO * 2)
    public void byTwoSeconds() {
        System.out.println("Executando método");
        System.out.println(LocalDateTime.now());
    }

}
